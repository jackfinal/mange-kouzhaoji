const Timestamp = new Date().getTime();

module.exports = {
  lintOnSave: false,
  publicPath: '/app/teachwork1/',
  devServer: {
    disableHostCheck: true,
    proxy: {
      '/campus': {
        target: 'https://api.yun.zhuoke.cn/',
        changeOrigin: true,
        secure: true,//https
        // ws: true,
        pathRequiresRewrite: {
          '^/campus': '/campus'
        }
      }
    }
  },
  configureWebpack: { // webpack 配置
    output: { // 输出重构  打包编译后的 文件名称  【模块名称.版本号.时间戳】
      filename: `js/[name].${process.env.VUE_APP_Version}.${Timestamp}.js`,
      chunkFilename: `js/[name].${process.env.VUE_APP_Version}.${Timestamp}.js`
    },
  },
}
