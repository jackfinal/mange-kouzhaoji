import Vue from 'vue'
import Vuex from 'vuex'
import { getApp, setApp } from '@/utils/app'
import { getUser, setUser } from '@/utils/user'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    app: getApp(),
  	user: getUser(),
    isadmin: -1
  },
  mutations: {
  	SET_APP: (state, app) => {
      setApp(app)
			state.app = app
		},
    SET_USER: (state, user) => {
      setUser(user)
      state.user = user
    },
    SET_ISADMIN: (state, isadmin) => {
      state.isadmin = isadmin
    }
  },
  actions: {
  	setAPP({ commit }, app) {
  	  commit('SET_APP', app)
  	},
    setUSER({ commit }, user) {
      commit('SET_USER', user)
    },
    setISADMIN({ commit }, isadmin) {
      commit('SET_ISADMIN', isadmin)
    }
  },
  modules: {
  }
})
