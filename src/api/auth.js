import request from '@/utils/request'


export function login(data) {
  return request({
    url: 'campus/login',
    method: 'post',
    data
  })
}