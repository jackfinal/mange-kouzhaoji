/*
* @Author: jack
* @Date:   2020-01-08 21:00:00
* @Last Modified by:   jack
* @Last Modified time: 2020-01-13 20:56:47
*/
import request from '@/utils/request'


export function getUser(data) {
  return request({
    url: 'campus/user/index',
    method: 'post',
    data
  })
}
export function getManageDepartList(data) {
  return request({
    url: 'campus/user/get/manage/depart/list',
    method: 'post',
    data
  })
}
export function getParentRelation(data) {
  return request({
    url: 'campus/user/get/parent/relation',
    method: 'post',
    data
  })
}
export function getParenetStudentExam(data) {
  return request({
    url: 'campus/user/get/parent/student/exam',
    method: 'get',
    data
  })
}