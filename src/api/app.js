/*
* @Author: jack
* @Date:   2020-01-05 16:45:08
* @Last Modified by:   jack
* @Last Modified time: 2020-10-27 19:36:39
*/
import request from '@/utils/request'
import { getToken } from '@/utils/auth'

export function upload(file, app_id) {
  let data = new FormData()
  data.append('file', file, file.name)
  data.append('app_id', app_id)

  // param.append('app_id', app_id)

  return request({
    headers: { 
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer ' + getToken()
    },
    url: 'campus/app/aetherupload/single/uploading',
    method: 'post',
    data
  })
}
export function add(data) {
  return request({
    url: 'campus/app/teachwork',
    method: 'post',
    data
  })
}
export function isAdmin(data) {
  return request({
    url: 'campus/app/teachwork/isadmin',
    method: 'post',
    data
  })
}
export function list(data) {
  return request({
    url: 'campus/app/teachwork',
    method: 'get',
    data
  })
}
export function show(id) {
  return request({
    url: 'campus/app/teachwork/' + id,
    method: 'get'
  })
}
export function deleteRow(id) {
  return request({
    url: 'campus/app/teachwork/' + id,
    method: 'delete'
  })
}
export function check(data) {
  return request({
    url: 'campus/app/teachwork/' + data.id,
    method: 'patch',
    data
  })
}
export function types(data) {
  return request({
    url: 'campus/app/teachwork/config',
    method: 'post',
    data
  })
}