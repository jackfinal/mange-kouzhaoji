import request from '@/utils/request'


export function resourceGetConfig(data) {
  return request({
    url: 'campus/app/resource/config',
    method: 'post',
    data
  })
}
export function resourceAdd(data) {
  return request({
    url: 'campus/app/resource/add',
    method: 'post',
    data
  })
}
export function resourceAddLogs(data) {
  return request({
    url: 'campus/app/resource/add/logs',
    method: 'post',
    data
  })
}
export function resourceIndex(data) {
  return request({
    url: 'campus/app/resource/index',
    method: 'post',
    data
  })
}
export function resourceInfo(data) {
  return request({
    url: 'campus/app/resource/info',
    method: 'post',
    data
  })
}
export function resourceMy(data) {
  return request({
    url: 'campus/app/resource/my',
    method: 'post',
    data
  })
}
export function resourceDelete(data) {
  return request({
    url: 'campus/app/resource/delete',
    method: 'post',
    data
  })
}
export function aetherUploadPreprocess(data) {
  return request({
    url: 'campus/app/aetherupload/preprocess',
    method: 'post',
    data
  })
}
export function getNoUserAttachment(data) {
  return request({
    url: 'campus/app/resource/nouser/attachment',
    method: 'post',
    data
  })
}
export function deleteAttachment(data) {
  return request({
    url: 'campus/app/resource/delete/attachment',
    method: 'post',
    data
  })
}