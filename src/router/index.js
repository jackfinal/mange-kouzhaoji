/*
* @Author: jack
* @Date:   2019-12-17 12:26:06
* @Last Modified by:   jack
* @Last Modified time: 2020-12-17 12:40:45
*/
import Vue from 'vue'
import Router from 'vue-router'
import Vant from 'vant';
import 'vant/lib/index.css';
import '../assets/app.css'

Vue.use(Router)
Vue.use(Vant)
let app = new Router({
  mode: 'history',
  base: '/app/teachwork1',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: () => import('@/pages/index'),
      meta: {
        requiresAuth: true,
        title: '教务工作'
      }
    },
    {
      path: '/pc',
      name: 'PC',
      component: () => import('@/pages/pc'),
      meta: {
        requiresAuth: true,
        title: '教务工作'
      }
    },
    {
      path: '/add',
      name: 'Add',
      component: () => import('@/pages/add'),
      meta: {
        requiresAuth: true,
        title: '新增-教务工作'
      }
    },
    {
      path: '/check',
      name: 'Check',
      component: () => import('@/pages/check'),
      meta: {
        requiresAuth: true,
        title: '待审-教务工作'
      }
    },
    {
      path: '/my',
      name: 'My',
      component: () => import('@/pages/my'),
      meta: {
        requiresAuth: true,
        title: '待审-教务工作'
      }
    },
    {
      path: '/info/:id',
      name: 'Info',
      component: () => import('@/pages/info'),
      meta: {
        requiresAuth: true,
        title: '详情-教务工作'
      }
    },
    {
      path: '/chart',
      name: 'Chart',
      component: () => import('@/pages/chart'),
      meta: {
        requiresAuth: true,
        title: '教务工作'
      }
    },
    {
      path: '/cover/:id/:name/:type',
      name: 'Cover',
      component: () => import('@/pages/cover'),
      meta: {
        requiresAuth: true,
        title: '教师-教务工作'
      }
    },
  ]
})

export default app

