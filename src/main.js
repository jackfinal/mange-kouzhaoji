import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import { getToken, setToken } from '@/utils/auth'
import { login } from '@/api/auth'
import 'vant/lib/icon/local.css';
import './assets/app.css';


Vue.config.productionTip = false


router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  let token = getToken()
  // let url_arr = window.location.href.split('#')
  // let url_arr1 = url_arr[0].split('?')
  //   if(url_arr.length >1 && url_arr1.length >1) {
  //     window.location.href = (url_arr1[0]  + '#' +url_arr[1] + '?' + url_arr1[1])
  // }

  if(to.query.userid) {
    // if(window.location.href.indexOf('mobile') > 0) {
    //   to.query.app_type = 'h5'
    //   to.query.app_id = to.params.app_id

    // }
    if(to.query.id && !to.query.app_id)to.query.app_id = to.query.id
    login(to.query).then(res=>{
      setToken(res.access_token)

      store.commit('SET_APP', res.app)
      store.commit('SET_USER', res.user)
      next()
    })
  }else{
    if(!token && to.meta.requiresAuth) {
      next()
    }else{
      next()
    }
  }
})
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
