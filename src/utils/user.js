import Cookies from 'js-cookie'

const TokenKey = 'zhihui-manage-user'

export function getUser() {
  let app = Cookies.get(TokenKey)
  if(app)return JSON.parse(app)
  return {}
}

export function setUser(token) {
  return Cookies.set(TokenKey, JSON.stringify(token))
}

export function removeUser() {
  return Cookies.remove(TokenKey)
}
