import Cookies from 'js-cookie'

const TokenKey = 'zhihui-manage-app'

export function getApp() {
  let app = Cookies.get(TokenKey)
  if(app)return JSON.parse(app)
  return {}
}

export function setApp(token) {
  return Cookies.set(TokenKey, JSON.stringify(token))
}

export function removeApp() {
  return Cookies.remove(TokenKey)
}

export function pushArray(list, add_list){
	for (let i in add_list) {
  		list.push(add_list[i])
  	}
  	return list
}