import axios from 'axios'
import { Dialog, Toast} from 'vant'
import store from '@/store'
import { getToken } from '@/utils/auth'
const API_ROOT = '/';
// create an axios instance
const service = axios.create({
  baseURL: API_ROOT, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    //add app_id
    if(store.state.app && config.url.indexOf('upload') < 0) {
      let app_id = store.state.app.id
      if(app_id)config.data = {app_id, ...config.data}
      if(config.method == 'get') {
        config.params = {app_id, ...config.data}
      }
    }
    Toast.loading({
      message: '加载中...',
      forbidClick: true,
    });
    config.headers['Authorization'] = 'Bearer ' + getToken()
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    Toast.clear();
    // if the custom code is not 20000, it is judged as an error.
    if (res.status !== 200) {
      Dialog.alert({
        message: res.message || 'Error',
        
      }).then(() => {
         if(res.status == 401)window.open(window.location.href, '_blank');
      });

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.status === 50008 || res.status === 50012 || res.status === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res.data
    }
  },
  error => {
    Toast.clear();
    console.log(error) // for debug
    Dialog({
      message: error.response.data.message
    }).then(() => {
        // if(error.response.status == 401 && window.location.href.indexOf('&opendnew=1') <0) {
        //   window.open(window.location.href + '&opendnew=1', '_blank')
        // }
    });
    return Promise.reject(error)
  }
)

export default service
