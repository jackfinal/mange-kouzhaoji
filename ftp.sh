#!/bin/bash
# @Author: jack
# @Date:   2020-06-19 16:29:09
# @Last Modified by:   jack
# @Last Modified time: 2020-11-04 16:37:08
echo "菜鸟教程：www.runoob.com"

host='121.40.141.144'
user='final'
passwd='ecyd1vToU7CCrPPi'
updir=`pwd`/dist
todir=/manage.yun.zhuoke.cn/app/teachwork1/
dirs=`gfind $updir -type d -printf $todir/'%P\n'| awk '{if ($0 == "")next;print "mkdir " $0}'`
files=`gfind $updir -type f -printf 'put %p %P \n'`
ftp  -nv  $host <<EOF
user ${user} ${passwd}
type binary
$dirs
cd $todir
$files
quit
EOF